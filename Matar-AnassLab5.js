let object = { a: 1, b: 2, c: "3" };
let array = [1, 2, 3];
//Problem 1:

const getProperties = (object) => {
  let array = [];
  Object.keys(object).forEach((key) => {
    array.push(key);
  });
  return array;
};
// console.log(getProperties({ a: 1, b: 2, c: "3" }));

//Problem 2:

const isPlainObject = (object) => {
  if (
    object != null &&
    Object.prototype.toString.call(object) === "[object Object]"
  ) {
    return true;
  }
  return false;
};

// console.log(isPlainObject({}));
//Problem 3:

const modifyObject = (object) => {
  if (isPlainObject(object)) {
    object.isObject = isPlainObject(object);
    object.getPropertiesNb = getProperties(object).length;
    return object;
  }
  return false;
};

// modifyObject(object);
// console.log(object);

//Problem 4:

const makePairs = (object) => {
  let array = [];
  for (const prop in object) {
    array.push([prop, object[prop]]);
  }
  return array;
};

// console.log(makePairs(object));

//Problem 5:

const without = (obj, ...arg) => {
  let props = arg;
  props.map((prop) => {
    if (prop in obj) {
      delete object[prop];
    }
  });
  return object;
};

// console.log(without(object,'a'))

// Problem 6:

const isEmpty = (object) => {
  if (getProperties(object).length == 0) {
    return true;
  } else {
    for (data in object) {
      if (object[data] !== undefined) {
        return false;
      }
    }
    return true;
  }
};

// console.log(isEmpty({o:undefined,a:undefined, f:4}))
// isEmpty({o:undefined,a:undefined,b:12})

//Problem 7:

const isEqual = (object1, object2) => {
  return Object.entries(object1).toString() === Object.entries(object2).toString();
};

// console.log(isEqual({a:1},{a:1}))


const intersection= (object1, object2) => {
  let arr = {}
  Object.keys(object1).filter((item) => {
      if(item in object2 && object1[item] == object2[item]){
        arr[item] = object1[item]
      }
  });
  return arr
}

console.log(intersection({a:1,b:2},{a:1,b:2,c:3,d:4}))